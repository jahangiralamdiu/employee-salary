package com.bs;

import java.util.Scanner;

public class SalaryManager {
    public static double companyAccountBalance;
    public static double initialBaseSalary;
    private final byte TOTAL_GRADE = 6;
    private final byte LOWEST_GRADE = 6;
    private final double SALARY_DIFFERENCE = 5000;
    private static double disbursedSalary;

    private Employee[] employees = new Employee[10];

    public void disburseSalary() {
        for (Employee employee : employees) {
            double salary = this.calculateSalary(employee.getGrade());
            this.checkAndFillBalance(salary);
            employee.setSalary(salary);
            employee.getBankAccount().addBalance(salary);
            companyAccountBalance -= salary;
            disbursedSalary += salary;
        }
    }

    private void checkAndFillBalance(double salaryAmount) {
        if (companyAccountBalance - salaryAmount > 0) {
            return;
        }

        System.out.println("Low account balance, please put some amount to company account: ");
        Scanner scanner = new Scanner(System.in);
        companyAccountBalance += scanner.nextDouble();
        checkAndFillBalance(salaryAmount);
    }

    public double calculateSalary(byte grade) {
        double baseSalary = ((LOWEST_GRADE - grade) * SALARY_DIFFERENCE) + initialBaseSalary;
        double allowance = this.calculateAllowance(baseSalary);
        double totalSalary = baseSalary + allowance;

        return totalSalary;
    }

    private double calculateAllowance(double baseSalary) {
        double medicalAllowance = baseSalary * .15; //15% of base
        double houseRent = baseSalary * .20; //20% of base
        return medicalAllowance + houseRent;
    }

    public void printEmployees() {
        for (Employee employee : employees) {
            System.out.println(employee.toString());
        }
    }

    public void printSalarySummary() {
        System.out.println("Total Salary Paid: " + disbursedSalary);
        System.out.println("Current company balance: " + companyAccountBalance);
    }

    //Block to initiate employee list
    {
        Employee employee1 = new Employee("Emp 1", (byte) 1, "Dhaka", "019221",
                new BankAccount("TBank", "Dhk", "9820881", "Current"));
        Employee employee2 = new Employee("Emp 2", (byte) 2, "Dhaka", "019222",
                new BankAccount("TBank", "Dhk", "9820882", "Current"));
        Employee employee3 = new Employee("Emp 3", (byte) 3, "Dhaka", "019223",
                new BankAccount("TBank", "Dhk", "9820883", "Current"));
        Employee employee4 = new Employee("Emp 4", (byte) 3, "Dhaka", "019224",
                new BankAccount("TBank", "Dhk", "9820884", "Current"));
        Employee employee5 = new Employee("Emp 5", (byte) 4, "Dhaka", "019225",
                new BankAccount("TBank", "Dhk", "9820885", "Current"));
        Employee employee6 = new Employee("Emp 6", (byte) 4, "Dhaka", "019226",
                new BankAccount("TBank", "Dhk", "9820886", "Current"));
        Employee employee7 = new Employee("Emp 7", (byte) 5, "Dhaka", "019227",
                new BankAccount("TBank", "Dhk", "9820887", "Current"));
        Employee employee8 = new Employee("Emp 8", (byte) 5, "Dhaka", "019228",
                new BankAccount("TBank", "Dhk", "9820888", "Current"));
        Employee employee9 = new Employee("Emp 8", (byte) 6, "Dhaka", "019229",
                new BankAccount("TBank", "Dhk", "9820889", "Current"));
        Employee employee10 = new Employee("Emp 10", (byte) 6, "Dhaka", "0192210",
                new BankAccount("TBank", "Dhk", "98208810", "Current"));
        employees[0] = employee1;
        employees[1] = employee2;
        employees[2] = employee3;
        employees[3] = employee4;
        employees[4] = employee5;
        employees[5] = employee6;
        employees[6] = employee7;
        employees[7] = employee8;
        employees[8] = employee9;
        employees[9] = employee10;
    }
}
