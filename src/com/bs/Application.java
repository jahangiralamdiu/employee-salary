package com.bs;

import java.util.Scanner;

public class Application {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter the basic salary of lowest grade: ");
        double basicSalary = scanner.nextDouble();

        System.out.println("Please add some money to company account: ");
        double companyBalance = scanner.nextDouble();

        SalaryManager.companyAccountBalance = companyBalance;
        SalaryManager.initialBaseSalary = basicSalary;

        SalaryManager salaryManager = new SalaryManager();
        salaryManager.disburseSalary();
        salaryManager.printEmployees();
        salaryManager.printSalarySummary();
    }
}
