package com.bs;

public class Employee {
    private String name;
    private byte grade;
    private String address;
    private String mobile;
    private BankAccount bankAccount;
    private double salary;

    public Employee() {
    }

    public Employee(String name, byte grade, String address, String mobile, BankAccount bankAccount) {
        this.name = name;
        this.grade = grade;
        this.address = address;
        this.mobile = mobile;
        this.bankAccount = bankAccount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public BankAccount getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(BankAccount bankAccount) {
        this.bankAccount = bankAccount;
    }

    public byte getGrade() {
        return grade;
    }

    public void setGrade(byte grade) {
        this.grade = grade;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "name='" + name + '\'' +
                ", grade=" + grade +
                ", salary=" + salary +
                ", address='" + address + '\'' +
                ", mobile='" + mobile + '\'' +
                ", bankAccount=" + bankAccount +
                '}';
    }
}
